package com.example.medias3.dto;

public interface IError {

    String getCode();
}
