package com.example.medias3.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Arrays;
import java.util.List;

public class ErrorModel {

//    @Schema(
//            description = "Http status code"
//    )
    private int statusCode;
//    @Schema(
//            description = "List error message"
//    )
    private List<String> messages;
    private String errorCode;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object additionalData;

    public ErrorModel(int statusCode, List<String> messages, String errorCode) {
        this(statusCode, messages);
        this.errorCode = errorCode;
    }

    public ErrorModel(int statusCode, List<String> messages) {
        this.statusCode = statusCode;
        this.messages = messages;
    }

    public ErrorModel(int statusCode, String message, String errorCode) {
        this(statusCode, message);
        this.errorCode = errorCode;
    }

    public ErrorModel(int statusCode, String message) {
        this.statusCode = statusCode;
        this.messages = Arrays.asList(message);
    }

    public int getStatusCode() {
        return this.statusCode;
    }

    public List<String> getMessages() {
        return this.messages;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public Object getAdditionalData() {
        return this.additionalData;
    }

    public void setStatusCode(final int statusCode) {
        this.statusCode = statusCode;
    }

    public void setMessages(final List<String> messages) {
        this.messages = messages;
    }

    public void setErrorCode(final String errorCode) {
        this.errorCode = errorCode;
    }

    public void setAdditionalData(final Object additionalData) {
        this.additionalData = additionalData;
    }

    public ErrorModel(final int statusCode, final List<String> messages, final String errorCode, final Object additionalData) {
        this.statusCode = statusCode;
        this.messages = messages;
        this.errorCode = errorCode;
        this.additionalData = additionalData;
    }

    public ErrorModel() {
    }

    public String toString() {
        int var10000 = this.getStatusCode();
        return "ErrorModel(statusCode=" + var10000 + ", messages=" + this.getMessages() + ", errorCode=" + this.getErrorCode() + ", additionalData=" + this.getAdditionalData() + ")";
    }
}
