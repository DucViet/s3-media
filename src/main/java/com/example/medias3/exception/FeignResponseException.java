package com.example.medias3.exception;

import com.example.medias3.dto.ErrorModel;
import org.springframework.http.HttpStatus;

public class FeignResponseException extends BaseCustomException {

    private HttpStatus status;
    private ErrorModel errorModel;

    public FeignResponseException(ErrorModel errorModel) {
        this.status = HttpStatus.valueOf(errorModel.getStatusCode());
        this.errorModel = errorModel;
    }

    public HttpStatus getHttpStatus() {
        return this.status;
    }

    public HttpStatus getStatus() {
        return this.status;
    }

    public ErrorModel getErrorModel() {
        return this.errorModel;
    }
}
