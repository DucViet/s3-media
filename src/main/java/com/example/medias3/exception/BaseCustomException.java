package com.example.medias3.exception;

import com.example.medias3.dto.IError;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class BaseCustomException extends RuntimeException {

    private List<String> messages;
    private String errorCode;
    private Map<IError, String> error;

    public abstract HttpStatus getHttpStatus();

    public BaseCustomException() {
    }

    public BaseCustomException(String... messages) {
        this.messages = Arrays.asList(messages);
    }

    public BaseCustomException(IError... errorCodes) {
        this.error = (Map)Arrays.asList(errorCodes).stream().collect(Collectors.toMap((e) -> {
            return e;
        }, (e) -> {
            return "";
        }));
    }

    public BaseCustomException(Map<IError, String> errorMap) {
        this.error = errorMap;
    }

    public List<String> getMessages() {
        return this.messages;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public Map<IError, String> getError() {
        return this.error;
    }

    public void setMessages(final List<String> messages) {
        this.messages = messages;
    }

    public void setErrorCode(final String errorCode) {
        this.errorCode = errorCode;
    }

    public void setError(final Map<IError, String> error) {
        this.error = error;
    }
}
