package com.example.medias3.domain;

import com.example.medias3.dto.MediaS3DTO;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import lombok.*;
import org.apache.commons.io.FilenameUtils;

import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "files")
@Data
@Builder
@AllArgsConstructor
@With
@NoArgsConstructor
public class File {
    @Id
    @Builder.Default
    private UUID id = UUID.randomUUID();
    private String name;
    private Long size;
    private String url;
    private String mimeType;
    private String extension;
    private Date createdDate;

    @PrePersist
    public void prePersist() {
        createdDate = new Date();
    }

    public static File fromMediaS3DTO(MediaS3DTO mediaS3DTO) {
        return File.builder()
                .name(mediaS3DTO.getOriginalFileName())
                .size((long) mediaS3DTO.getBytes().length)
                .mimeType(mediaS3DTO.getContentType())
                .extension(FilenameUtils.getExtension(mediaS3DTO.getFileName()))
                .createdDate(new Date())
                .build();
    }
}