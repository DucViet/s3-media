package com.example.medias3.utils;

import com.example.medias3.config.StorageConfiguration;
import com.example.medias3.dto.MediaS3DTO;
import com.example.medias3.statics.StorageType;
import org.apache.commons.lang3.StringUtils;

public class StorageUtils {
    public static String buildFileUrl(String key) {
        return PathUtils.join(StorageConfiguration.ENDPOINT, key);
    }

    public static String buildKey(StorageType storageType, MediaS3DTO mediaS3DTO) {
        if (StringUtils.isBlank(mediaS3DTO.getFolderName())) {
            return PathUtils.join(mediaS3DTO.getAccessTypeFolder(), storageType.getNameLower(), mediaS3DTO.getFileName());
        }
        return PathUtils.join(mediaS3DTO.getAccessTypeFolder(), storageType.getNameLower(), mediaS3DTO.getFolderName(), mediaS3DTO.getFileName());
    }

    public static String buildKey(MediaS3DTO mediaS3DTO) {
        if (StringUtils.isBlank(mediaS3DTO.getFolderName())) {
            return PathUtils.join(mediaS3DTO.getAccessTypeFolder(), mediaS3DTO.getFileName());
        }
        return PathUtils.join(mediaS3DTO.getAccessTypeFolder(), mediaS3DTO.getFolderName(), mediaS3DTO.getFileName());
    }
}
