package com.example.medias3.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class FileUtils {

    private final static String FORMAT_DATE = "dd_MM_yyyy";
    private final static String FILE_NAME = "File";

    public static String generateFileName(String fileExtension) {
        String dateFormat = new SimpleDateFormat(FORMAT_DATE).format(new Date());
        return FILE_NAME + "_" + dateFormat + "_" + new Date().getTime() + "." + fileExtension;
    }

}
