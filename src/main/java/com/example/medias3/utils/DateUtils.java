package com.example.medias3.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

public class DateUtils {

    public static Date getDateAfter(long seconds) {
        long expTimeMillis = new Date().getTime();
        expTimeMillis += seconds * 1000;
        return new Date(expTimeMillis);
    }

    public static Integer calculateAge(Date startDateInput, Date endDateInput) {
        LocalDate startDate = startDateInput.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endDate = endDateInput.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return Period.between(startDate, endDate).getYears();
    }

    public static Integer calculateAge(LocalDate startDateInput, LocalDate endDateInput) {
        return Period.between(startDateInput, endDateInput).getYears();
    }

    public static Date getDateGMT7(Date date){
        LocalDateTime localDateTime = date.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime().plusHours(7);
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}
