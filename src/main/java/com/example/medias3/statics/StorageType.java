package com.example.medias3.statics;

import java.util.Locale;

public enum StorageType {
    TEMPORARY, PERMANENT;

    public String getNameLower() {
        return toString().toLowerCase(Locale.ROOT);
    }
}
