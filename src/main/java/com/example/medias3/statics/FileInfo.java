package com.example.medias3.statics;

import java.util.Arrays;
import java.util.List;

public class FileInfo {

    public static final List<String> FILE_EXCEPT = Arrays.asList("pdf", "PDF", "JPEG", "jpeg", "JPG", "jpg", "PNG", "png");
    public static final List<String> FILE_EXCEPT_PDF = Arrays.asList("pdf", "PDF");
    public static final long FILE_SIZE_MAX = 5000000;
}
