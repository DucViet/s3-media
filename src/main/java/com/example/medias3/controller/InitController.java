package com.example.medias3.controller;

import com.example.medias3.domain.File;
import com.example.medias3.service.InitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api")
public class InitController {

    @Autowired
    InitService initService;

    @PostMapping("/file/upload")
    public File uploadAttachFile(@RequestParam(value = "file") MultipartFile attachFile) {
        return initService.uploadAttachFile(attachFile);
    }
}
