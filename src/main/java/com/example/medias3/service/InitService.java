package com.example.medias3.service;

import com.example.medias3.domain.File;
import com.example.medias3.dto.MediaS3DTO;
import com.example.medias3.exception.NotFoundException;
import com.example.medias3.statics.FileInfo;
import com.example.medias3.statics.ResponseMessage;
import com.example.medias3.statics.StorageType;
import com.example.medias3.utils.FileUtils;
import lombok.SneakyThrows;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class InitService {

    @Autowired
    private FileService fileService;

    @SneakyThrows
    public File uploadAttachFile(MultipartFile attachFile) {
        String fileExtension = FilenameUtils.getExtension(attachFile.getOriginalFilename());
        String fileNameUploadS3 = FileUtils.generateFileName(fileExtension);
        String fileNameOriginal = attachFile.getOriginalFilename();
        return this.uploadAttachFileS3(fileExtension, attachFile.getBytes(), fileNameUploadS3, fileNameOriginal);
    }

    public File uploadAttachFileS3(String fileExtension, byte[] attachFile, String fileNameUploadS3, String fileNameOriginal) {
        MediaS3DTO mediaS3DTO = createMediaS3DTO(fileExtension, attachFile, fileNameUploadS3, fileNameOriginal);
        return fileService.uploadFile(mediaS3DTO, StorageType.PERMANENT).orElseThrow(() -> new NotFoundException(ResponseMessage.FILE_NOT_FOUND));
    }

    public MediaS3DTO createMediaS3DTO(String fileExtension, byte[] attachFile, String fileNameUploadS3, String fileNameOriginal) {
        MediaS3DTO mediaS3DTO = new MediaS3DTO();
        mediaS3DTO.setBytes(attachFile);
        mediaS3DTO.setOriginalFileName(fileNameOriginal);
        mediaS3DTO.setFileName(fileNameUploadS3);
        mediaS3DTO.setPublicRead(false);
        if (fileExtension != null && FileInfo.FILE_EXCEPT_PDF.contains(fileExtension)) {
            mediaS3DTO.setContentType(MediaS3DTO.APPLICATION_PDF);
        } else mediaS3DTO.setContentType(MediaS3DTO.TEXT_PLAIN);
        return mediaS3DTO;
    }
}
